from tastypie.authentication import *

from django_cdstack_deploy.django_cdstack_deploy.models import *


class UserApiKeyAuthentication(ApiKeyAuthentication):
    auth_type = "apikey"

    def is_authenticated(self, request, **kwargs):
        """
        Finds the user and checks their API key.

        Should return either ``True`` if allowed, ``False`` if not or an
        ``HttpResponse`` if you need something custom.
        """
        try:
            username, api_key = self.extract_credentials(request)
        except ValueError:
            return self._unauthorized()

        if not username or not api_key:
            return self._unauthorized()

        try:
            user = CmdbApiKeyUser.objects.get(user__username=username, key=api_key).user
        except:
            return self._unauthorized()

        key_auth_check = self.get_key(user, api_key)
        if key_auth_check and not isinstance(key_auth_check, HttpUnauthorized):
            request.user = user

        return key_auth_check

    def get_key(self, user, api_key):
        try:
            if user.api_key.key != api_key:
                return self._unauthorized()
        except CmdbApiKeyUser.DoesNotExist:
            return self._unauthorized()

        return True


class HostApiKeyAuthentication(ApiKeyAuthentication):
    auth_type = "apikey"

    def is_authenticated(self, request, **kwargs):
        """
        Finds the user and checks their API key.

        Should return either ``True`` if allowed, ``False`` if not or an
        ``HttpResponse`` if you need something custom.
        """
        try:
            username, api_key = self.extract_credentials(request)
        except ValueError:
            return self._unauthorized()

        if not username or not api_key:
            return self._unauthorized()

        try:
            user = CmdbHost.objects.select_related("api_key").get(id=username)
        except (CmdbHost.DoesNotExist, CmdbHost.MultipleObjectsReturned):
            return self._unauthorized()

        key_auth_check = self.get_key(user, api_key)
        if key_auth_check and not isinstance(key_auth_check, HttpUnauthorized):
            request.user = user

        return key_auth_check

    def get_key(self, user, api_key):
        try:
            if user.api_key.key != api_key:
                return self._unauthorized()
        except CmdbApiKeyHost.DoesNotExist:
            return self._unauthorized()

        return True


class InstanceApiKeyAuthentication(ApiKeyAuthentication):
    auth_type = "apikey"

    def is_authenticated(self, request, **kwargs):
        """
        Finds the user and checks their API key.

        Should return either ``True`` if allowed, ``False`` if not or an
        ``HttpResponse`` if you need something custom.
        """
        try:
            username, api_key = self.extract_credentials(request)
        except ValueError:
            return self._unauthorized()

        if not username or not api_key:
            return self._unauthorized()

        try:
            user = CmdbInstance.objects.select_related("api_key").get(id=username)
        except:
            return self._unauthorized()

        key_auth_check = self.get_key(user, api_key)
        if key_auth_check and not isinstance(key_auth_check, HttpUnauthorized):
            request.user = user

        return key_auth_check

    def get_key(self, user, api_key):
        try:
            if user.api_key.key != api_key:
                return self._unauthorized()
        except CmdbApiKeyInstance.DoesNotExist:
            return self._unauthorized()

        return True


class AnonymousGetAuthentication(MultiAuthentication):
    def is_authenticated(self, request, **kwargs):
        """
        Identifies if the user is authenticated to continue or not.

        Should return either ``True`` if allowed, ``False`` if not or an
        ``HttpResponse`` if you need something custom.
        """
        authorized = False

        for backend in self.backends:
            check = backend.is_authenticated(request, **kwargs)

            if check:
                if isinstance(check, HttpUnauthorized):
                    authorized = authorized or check
                else:
                    request._authentication_backend = backend
                    return check

        if request.method == "GET":
            authorized = True

        return authorized
