from django.db.models import signals
from tastypie.models import ApiKey as TastypieApiKey

from django_cdstack_models.django_cdstack_models.models import *


class CmdbApiKeyUser(TastypieApiKey):
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL, related_name="api_key", on_delete=models.CASCADE
    )
    key = models.CharField(max_length=128, blank=True, default="", db_index=True)
    created = models.DateTimeField(default=datetime.now)

    def create_api_key(sender, instance, created, **kwargs):
        """
        A signal for hooking up automatic ``ApiKey`` creation.
        """
        if kwargs.get("raw", False) is False and created is True:
            CmdbApiKeyUser.objects.create(user=instance)

    class Meta:
        abstract = False
        db_table = "cmdb_api_key_user"


class CmdbApiKeyHost(TastypieApiKey):
    user = models.OneToOneField(
        CmdbHost, related_name="api_key", on_delete=models.CASCADE
    )
    key = models.CharField(max_length=128, blank=True, default="", db_index=True)
    created = models.DateTimeField(default=datetime.now)

    def create_api_key(sender, instance, created, **kwargs):
        """
        A signal for hooking up automatic ``ApiKey`` creation.
        """
        if kwargs.get("raw", False) is False and created is True:
            CmdbApiKeyHost.objects.create(user=instance)

    class Meta:
        abstract = False
        db_table = "cmdb_api_key_host"


class CmdbApiKeyInstance(TastypieApiKey):
    user = models.OneToOneField(
        CmdbInstance, related_name="api_key", on_delete=models.CASCADE
    )
    key = models.CharField(max_length=128, blank=True, default="", db_index=True)
    created = models.DateTimeField(default=datetime.now)

    def create_api_key(sender, instance, created, **kwargs):
        """
        A signal for hooking up automatic ``ApiKey`` creation.
        """
        if kwargs.get("raw", False) is False and created is True:
            CmdbApiKeyInstance.objects.create(user=instance)

    class Meta:
        abstract = False
        db_table = "cmdb_api_key_instance"


if settings.PROJECT_NAME == "django-cdstack":
    signals.post_save.connect(
        CmdbApiKeyUser.create_api_key, sender=settings.AUTH_USER_MODEL
    )
    signals.post_save.connect(CmdbApiKeyHost.create_api_key, sender=CmdbHost)
    signals.post_save.connect(CmdbApiKeyInstance.create_api_key, sender=CmdbInstance)
