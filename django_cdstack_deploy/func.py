import os
import zipfile
import ipaddress
import json
from urllib.parse import urlparse
from collections import OrderedDict

from django.template import engines
from datetime import datetime

from django_cdstack_deploy.django_cdstack_deploy.models import *


def traverse_childgroups(cmdb_group: CmdbGroup, parent_groups=None, level: int = 0):
    if parent_groups is None:
        parent_groups = dict()

    parents = CmdbChildgroups.objects.filter(child_rel=cmdb_group)

    if len(parents) > 0:
        if level not in parent_groups:
            parent_groups[level] = list()

        for parent_group in parents:
            parent_groups[level].append(parent_group.parent_rel)
            traverse_childgroups(parent_group.parent_rel, parent_groups, level + 1)

    return parent_groups


def traverse_childgroup_vars(parent_groups: dict):
    template_opts = dict()

    for key, group_list in sorted(parent_groups.items(), reverse=True):
        for group in group_list:
            for variable in group.cmdbvarsgroup_set.all().order_by("name"):
                try:
                    template_opts[variable.name] = variable.value.replace("\r\n", "\n")
                except AttributeError:
                    template_opts[variable.name] = variable.value

    return template_opts


def get_host_vars(cmdb_host: CmdbHost):
    template_opts = OrderedDict()

    template_opts["cmdb_host"] = cmdb_host

    template_opts["node_hostname_fqdn"] = cmdb_host.hostname
    template_opts["node_hostname"] = cmdb_host.hostname.split(".")[0]

    try:
        api_key = cmdb_host.api_key
    except CmdbApiKeyHost.DoesNotExist:
        api_key = CmdbApiKeyHost(user=cmdb_host)
        api_key.save()

    template_opts["cmdb_api_username"] = str(cmdb_host.id)
    template_opts["cmdb_api_password"] = str(api_key.key)

    # Apply global defaults
    for variable in CmdbVarsGlobalDefault.objects.all().order_by("name"):
        try:
            template_opts[variable.name] = variable.value.replace("\r\n", "\n")
        except AttributeError:
            template_opts[variable.name] = variable.value

    # Apply variables attached to instance
    for variable in cmdb_host.inst_rel.cmdbvarsinstance_set.all().order_by("name"):
        try:
            template_opts[variable.name] = variable.value.replace("\r\n", "\n")
        except AttributeError:
            template_opts[variable.name] = variable.value

    # Apply variables attached to location and master location
    if cmdb_host.location_rel is not None:
        if cmdb_host.location_rel.location_master is not None:
            for (
                variable
            ) in cmdb_host.location_rel.location_master.cmdbvarslocationmaster_set.all().order_by(
                "name"
            ):
                try:
                    template_opts[variable.name] = variable.value.replace("\r\n", "\n")
                except AttributeError:
                    template_opts[variable.name] = variable.value

        for variable in cmdb_host.location_rel.cmdbvarslocation_set.all().order_by(
            "name"
        ):
            try:
                template_opts[variable.name] = variable.value.replace("\r\n", "\n")
            except AttributeError:
                template_opts[variable.name] = variable.value

    # Apply variables attached to group
    for host_group in cmdb_host.cmdbhostgroups_set.all().order_by("apply_order"):
        # Merge Childgroups
        template_opts_childgroups = traverse_childgroup_vars(
            traverse_childgroups(host_group.group_rel)
        )
        template_opts = {**template_opts, **template_opts_childgroups}

        for variable in host_group.group_rel.cmdbvarsgroup_set.all().order_by("name"):
            try:
                template_opts[variable.name] = variable.value.replace("\r\n", "\n")
            except AttributeError:
                template_opts[variable.name] = variable.value

    # Apply variables attached to host
    for variable in cmdb_host.cmdbvarshost_set.all().order_by("name"):
        try:
            template_opts[variable.name] = variable.value.replace("\r\n", "\n")
        except AttributeError:
            template_opts[variable.name] = variable.value

    if "network_iface_primary" not in template_opts:
        template_opts["network_iface_primary"] = "eth0"

    if (
        "network_iface_" + template_opts["network_iface_primary"] + "_ip"
        in template_opts
    ):
        template_opts["network_iface_primary_ip"] = template_opts[
            "network_iface_" + template_opts["network_iface_primary"] + "_ip"
        ]

    if (
        "network_iface_" + template_opts["network_iface_primary"] + "_ip6"
        in template_opts
    ):
        template_opts["network_iface_primary_ip6"] = template_opts[
            "network_iface_" + template_opts["network_iface_primary"] + "_ip6"
        ]

    if (
        "network_iface_" + template_opts["network_iface_primary"] + "_gw"
        in template_opts
    ):
        template_opts["network_iface_primary_gw"] = template_opts[
            "network_iface_" + template_opts["network_iface_primary"] + "_gw"
        ]

    template_opts["monitoring_snmp_trap_receivers"] = get_snmp_trap_receivers(
        template_opts
    )
    template_opts["monitoring_syslog_receivers"] = get_syslog_receivers(template_opts)

    if len(template_opts["monitoring_snmp_trap_receivers"]) < 1:
        if "monitoring_snmp_trap_receiver_default" in template_opts:
            if (
                template_opts["monitoring_snmp_trap_receiver_default"]
                == "default_gateway"
                and "network_iface_primary_gw" in template_opts
            ):
                template_opts["monitoring_snmp_trap_receiver_01_ip"] = template_opts[
                    "network_iface_primary_gw"
                ]

            template_opts["monitoring_snmp_trap_receivers"] = get_snmp_trap_receivers(
                template_opts
            )

    if len(template_opts["monitoring_syslog_receivers"]) < 1:
        if "monitoring_syslog_receiver_default" in template_opts:
            if (
                template_opts["monitoring_syslog_receiver_default"] == "default_gateway"
                and "network_iface_primary_gw" in template_opts
            ):
                template_opts["monitoring_syslog_receiver_01_ip"] = template_opts[
                    "network_iface_primary_gw"
                ]

            template_opts["monitoring_syslog_receivers"] = get_syslog_receivers(
                template_opts
            )

    template_opts["fw_white_list_global"] = json.loads(
        template_opts["fw_white_list_feed_srvfarm"]
    )

    template_opts["fw_white_list_ips"] = list()
    template_opts["fw_white_list_ip6s"] = list()

    template_opts["fail2ban_white_list_ips"] = list()
    template_opts["fail2ban_white_list_ip6s"] = list()

    for fw_white_service in template_opts["fw_white_list_global"]:
        for fw_white_ip4 in fw_white_service["ip4"]:
            template_opts["fw_white_list_ips"].append(
                str(ipaddress.ip_network(fw_white_ip4, strict=False))
            )
        for fw_white_ip6 in fw_white_service["ip6"]:
            template_opts["fw_white_list_ip6s"].append(
                str(ipaddress.ip_network(fw_white_ip6, strict=False))
            )
        for fw_white_network4 in fw_white_service["network4"]:
            template_opts["fw_white_list_ips"].append(
                str(ipaddress.ip_network(fw_white_network4, strict=False))
            )
        for fw_white_network6 in fw_white_service["network6"]:
            template_opts["fw_white_list_ip6s"].append(
                str(ipaddress.ip_network(fw_white_network6, strict=False))
            )

    for fw_white_ip_key, fw_white_ip_val in template_opts.items():
        if fw_white_ip_key.startswith("fw_white_list_ip_"):
            template_opts["fw_white_list_ips"].append(
                str(ipaddress.ip_network(fw_white_ip_val, strict=False))
            )

    for fw_white_ip6_key, fw_white_ip6_val in template_opts.items():
        if fw_white_ip6_key.startswith("fw_white_list_ip6_"):
            template_opts["fw_white_list_ip6s"].append(
                str(ipaddress.ip_network(fw_white_ip6_val, strict=False))
            )

    for fail2ban_white_ip_key, fail2ban_white_ip_val in template_opts.items():
        if fail2ban_white_ip_key.startswith("fail2ban_white_list_ip_"):
            template_opts["fail2ban_white_list_ips"].append(
                str(ipaddress.ip_network(fail2ban_white_ip_val, strict=False))
            )

    for fail2ban_white_ip6_key, fail2ban_white_ip6_val in template_opts.items():
        if fail2ban_white_ip6_key.startswith("fail2ban_white_list_ip6_"):
            template_opts["fail2ban_white_list_ip6s"].append(
                str(ipaddress.ip_network(fail2ban_white_ip6_val, strict=False))
            )

    # remove duplicates from whitelists
    template_opts["fw_white_list_ips"] = list(
        dict.fromkeys(template_opts["fw_white_list_ips"])
    )
    template_opts["fw_white_list_ip6s"] = list(
        dict.fromkeys(template_opts["fw_white_list_ip6s"])
    )
    template_opts["fail2ban_white_list_ips"] = list(
        dict.fromkeys(template_opts["fail2ban_white_list_ips"])
    )
    template_opts["fail2ban_white_list_ip6s"] = list(
        dict.fromkeys(template_opts["fail2ban_white_list_ip6s"])
    )

    return template_opts


def get_snmp_trap_receivers(template_opts):
    monitoring_snmp_trap_receivers = list()

    for key in template_opts.keys():
        if key.startswith("monitoring_snmp_trap_receiver_") and key.endswith("_ip"):
            key_ident = key[:-3]

            monitoring_snmp_trap_receiver = dict()

            monitoring_snmp_trap_receiver["ip"] = template_opts[key_ident + "_ip"]

            if key_ident + "_port" in template_opts:
                monitoring_snmp_trap_receiver["port"] = template_opts[
                    key_ident + "_port"
                ]
            else:
                monitoring_snmp_trap_receiver["port"] = "162"

            monitoring_snmp_trap_receivers.append(monitoring_snmp_trap_receiver)

    return monitoring_snmp_trap_receivers


def get_syslog_receivers(template_opts):
    monitoring_syslog_receivers = list()

    for key in template_opts.keys():
        if key.startswith("monitoring_syslog_receiver_") and key.endswith("_ip"):
            key_ident = key[:-3]

            monitoring_syslog_receiver = dict()

            monitoring_syslog_receiver["ip"] = template_opts[key_ident + "_ip"]

            if key_ident + "_port" in template_opts:
                monitoring_syslog_receiver["port"] = template_opts[key_ident + "_port"]
            else:
                monitoring_syslog_receiver["port"] = "514"

            monitoring_syslog_receivers.append(monitoring_syslog_receiver)

    return monitoring_syslog_receivers


def zip_add_file(zipfile_handler, dest, data):
    if dest.startswith("/"):
        dest = dest[1:]

    dest = dest.replace("\\", "/")

    if hasattr(zipfile_handler, "blacklist"):
        if dest in zipfile_handler.blacklist:
            return False

    if dest not in zipfile_handler.namelist():
        zipfile_handler.writestr(dest, data, zipfile.ZIP_DEFLATED)

    return True


def generate_motd(cmdb_host: CmdbHost, template_opts):
    from art import text2art

    motd = str()
    motd += text2art("CMDB")
    motd += text2art(
        cmdb_host.cmdbhostgroups_set.all()
        .order_by("apply_order")
        .first()
        .group_rel.config_pkg.upper()
    )
    motd += "=========================================================================================\n"
    motd += "CMDB-Operator: {}\n".format(template_opts["cmdb_operator"])
    motd += "CMDB-Operator-Contact: {}\n".format(template_opts["cmdb_operator_contact"])
    motd += f"CMDB-Instance: {cmdb_host.inst_rel.name} (ID: {cmdb_host.inst_rel_id})\n"
    motd += f"CMDB-Host-ID: {cmdb_host.id}\n"
    motd += "Last Deployment: {}\n".format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
    motd += "=========================================================================================\n"

    return motd


def generate_config_static(zipfile_handler, template_opts, module_path):
    django_engine = engines["django"]

    config_path = module_path + "/templates/config-fs/static/"
    config_path_dlen = len(config_path)

    if os.path.isdir(config_path):
        for root, dirs, files in os.walk(config_path):
            for file in files:
                try:
                    config_template_file = open(os.path.join(root, file), "r").read()
                except FileNotFoundError:
                    # maybe broken symlink
                    continue

                config_template = django_engine.from_string(config_template_file)

                rel = root[config_path_dlen:]
                dest = os.path.join(rel, file)

                zip_add_file(
                    zipfile_handler, dest, config_template.render(template_opts)
                )

    # copy other static files
    copy_path = module_path + "/templates/config-fs/copy/"
    copy_path_dlen = len(copy_path)
    if os.path.isdir(copy_path):
        for root, dirs, files in os.walk(copy_path):
            for file in files:
                try:
                    original_file = open(os.path.join(root, file), "rb").read()
                except FileNotFoundError:
                    # maybe broken symlink
                    continue

                rel = root[copy_path_dlen:]
                dest = os.path.join(rel, file)

                zip_add_file(zipfile_handler, dest, original_file)

    # image-copy other static files
    image_copy_path = module_path + "/templates/image/copy-to-filesystem/"
    image_copy_path_dlen = len(image_copy_path)
    if os.path.isdir(image_copy_path):
        for root, dirs, files in os.walk(image_copy_path):
            for file in files:
                try:
                    original_file = open(os.path.join(root, file), "rb").read()
                except FileNotFoundError:
                    # maybe broken symlink
                    continue

                rel = root[image_copy_path_dlen:]
                dest = os.path.join(rel, file)

                zip_add_file(zipfile_handler, dest, original_file)

    return True


def extract_hostname(input: str):
    parts = input.split()

    for part in parts:
        if part.startswith("http://") or part.startswith("https://"):
            parsed_url = urlparse(part)
            return parsed_url.hostname

    return False
