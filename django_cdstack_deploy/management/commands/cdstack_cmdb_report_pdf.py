import os
import tempfile
from pprint import pprint

from django.core.management.base import BaseCommand
from reportlab.lib.colors import yellow
from reportlab.lib.pagesizes import A4, portrait
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.platypus import SimpleDocTemplate, Table, Paragraph

from django_cdstack_models.django_cdstack_models.models import *


class Command(BaseCommand):
    help = "Generate reports for CMDB"

    data = dict()

    new_line = "|- "

    def list_config(self, host):
        for var_row in host.cmdbvarshost_set.all():
            cdrline = [var_row.name, var_row.value[0:99]]
            self.data[host.id].append(cdrline)

        return True

    def handle(self, *args, **options):
        self.stdout.write(self.help)

        temppath = tempfile.mkdtemp()

        all_hosts = CmdbHost.objects.all()

        for host in all_hosts:
            self.data[host.id] = []
            doc = SimpleDocTemplate(
                os.path.join(temppath, "host_" + str(host.id) + ".pdf"),
                pagesize=portrait(A4),
            )

            styles = getSampleStyleSheet()

            styles.add(ParagraphStyle("default", backColor=yellow))

            # container for the 'Flowable' objects
            elements = []

            title_data = host.hostname

            table_header = ["VAR", "VALUE"]
            self.data[host.id].append(table_header)

            self.list_config(host)

            t = Table(self.data[host.id])
            title = Paragraph(title_data, styles["default"])
            # t.setStyle(TableStyle([('BACKGROUND', (1, 1), (-2, -2), colors.green),
            #                       ('TEXTCOLOR', (0, 0), (1, -1), colors.red)]))
            elements.append(title)
            elements.append(t)
            # write the document to disk
            doc.build(elements)

        self.stdout.write(self.style.SUCCESS("Successfully finished."))
