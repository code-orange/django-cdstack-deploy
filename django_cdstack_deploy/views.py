import base64
import zipfile
from importlib import import_module
from io import BytesIO
from wsgiref.util import FileWrapper

from django.http import HttpResponse

from django_cdstack_deploy.django_cdstack_deploy.func import get_host_vars
from django_cdstack_models.django_cdstack_models.models import *


def handle(request):
    # Check for valid basic auth header
    if "HTTP_AUTHORIZATION" in request.META:
        auth = request.META["HTTP_AUTHORIZATION"].split()
        if len(auth) == 2:
            if auth[0].lower() == "basic":
                uname, passwd = base64.b64decode(auth[1]).decode("utf-8").split(":")

                try:
                    uname = int(uname)
                except ValueError:
                    pass
                else:
                    try:
                        host = CmdbHost.objects.get(id=uname, api_key__key=passwd)
                    except CmdbHost.DoesNotExist:
                        try:
                            instance = CmdbInstance.objects.get(
                                id=uname, api_key__key=passwd
                            )
                        except CmdbInstance.DoesNotExist:
                            pass
                        else:
                            return deploy(request, instance)
                    else:
                        return deploy(request, host.inst_rel)

    # Either they did not provide an authorization header or
    # something in the authorization attempt failed. Send a 401
    # back to them to ask them to authenticate.
    response = HttpResponse()
    response.status_code = 401
    response["WWW-Authenticate"] = 'Basic realm="DEPLOY AUTH"'
    return response


def deploy(request, instance):
    nodeid = request.GET.get("nodeid", "missing")
    nodetype = request.GET.get("nodetype", "missing")

    if nodeid == "missing":
        return HttpResponse("Authorization Error", status=401)

    if nodetype == "missing":
        return HttpResponse("Authorization Error", status=401)

    zipfile_io = BytesIO()
    zipfile_handler = zipfile.ZipFile(zipfile_io, "w", zipfile.ZIP_DEFLATED)

    if not template_cmdb(request, zipfile_handler, nodeid, instance):
        return HttpResponse("Node config incomplete!", status=500)

    zipfile_handler.close()
    zipfile_io.seek(0)

    response = HttpResponse(FileWrapper(zipfile_io), content_type="application/zip")
    response["Content-Disposition"] = "attachment; filename=nodeconfig.zip"
    return response


def template_cmdb(request, zipfile_handler, nodeid, instance):
    try:
        cmdb_host = CmdbVarsHost.objects.get(
            name="network_iface_eth0_hwaddr", value=nodeid, host_rel__inst_rel=instance
        ).host_rel
    except CmdbVarsHost.DoesNotExist:
        return HttpResponse("Authorization Error", status=401)

    client_os_family = request.GET.get("os_family", "unknown")
    client_os_version = request.GET.get("os_version", "unknown")

    if not client_os_family == "unknown" and not client_os_version == "unknown":
        # Update OS
        try:
            os_family = CmdbOsFamily.objects.get(name__iexact=client_os_family)
        except CmdbOsFamily.DoesNotExist:
            pass
        else:
            try:
                os_version = CmdbOsVersion.objects.get(
                    family=os_family, name__iexact=client_os_version
                )
            except CmdbOsVersion.DoesNotExist:
                os_version = CmdbOsVersion(family=os_family, name=client_os_version)
                os_version.save(force_insert=True)

            if not cmdb_host.os == os_version:
                cmdb_host.os = os_version
                cmdb_host.save()

    template_opts = get_host_vars(cmdb_host)

    cmdb_host.host_last_fetch = datetime.now()
    cmdb_host.save()

    cmdb_host.refresh_from_db()

    host_groups = cmdb_host.cmdbhostgroups_set.all().order_by("apply_order")

    for host_group in host_groups:
        config_pkg_name = host_group.group_rel.config_pkg
        config_pkg = import_module(
            "django_cdstack_tpl_"
            + config_pkg_name
            + ".django_cdstack_tpl_"
            + config_pkg_name
            + ".views"
        )

        config_pkg.handle(
            zipfile_handler, template_opts, cmdb_host, skip_handle_os=False
        )

    # clear dict
    template_opts = None

    return True
