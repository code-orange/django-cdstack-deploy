from django.core.management.base import BaseCommand
from django_cdstack_models.django_cdstack_models.models import CmdbVarsHost, CmdbHost
import json
from pprint import pprint


class Command(BaseCommand):
    help = "Import nodeconfig into CMDB"

    def add_arguments(self, parser):
        parser.add_argument("node_id", type=int)

    def handle(self, *args, **options):
        self.stdout.write(self.help)

        node_id = options["node_id"]

        # load file
        json_file = open("nodeconfig.json", "r").read()

        json_data = json.loads(json_file)

        for key, value in json_data.items():
            try:
                val = CmdbVarsHost.objects.get(host_rel=node_id, name=key)
            except CmdbVarsHost.DoesNotExist:
                val = CmdbVarsHost(
                    host_rel=CmdbHost.objects.get(id=node_id), name=key, value=value
                )
                val.save()

            val.value = value
            val.save()

        self.stdout.write("FINISHED!")
