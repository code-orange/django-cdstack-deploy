from django.urls import path

from . import views

urlpatterns = [
    path("getconfig", views.handle),
    path("v1/getconfig", views.handle),
]
